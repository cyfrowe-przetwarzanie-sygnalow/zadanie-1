package pl.pmms.cps.controllers;


import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import pl.pmms.cps.model.*;
import pl.pmms.cps.model.converters.ConverterAC;
import pl.pmms.cps.model.converters.ConverterCA;
import pl.pmms.cps.model.noises.*;
import pl.pmms.cps.model.signals.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ExampleChartController {

    @FXML
    private Pane exampleChartPane;
    @FXML
    private LineChart<Number, Number> lineChart2;
    @FXML
    private BarChart<String, Number> barChart1;
    @FXML
    private ComboBox signalChoice;
    @FXML
    private TextField amplitudeValue;
    @FXML
    private TextField periodValue;
    @FXML
    private TextField beginTimeValue;
    @FXML
    private TextField durationTimeValue;
    @FXML
    private TextField linerValue;
    @FXML
    private TextField samplingValue;
    @FXML
    private TextField stepValue;
    @FXML
    private TextField probabilityValue;
    @FXML
    private Slider rangesAmountSlider;
    @FXML
    private TextField averageValueField;
    @FXML
    private TextField averageAbsoluteValueField;
    @FXML
    private TextField efficiencyValueField;
    @FXML
    private TextField varianceField;
    @FXML
    private TextField averagePowerField;
    @FXML
    private ComboBox signalsList;
    @FXML
    private ComboBox firstSignalList;
    @FXML
    private ComboBox secondSignalList;
    @FXML
    private ComboBox operationPicker;
    @FXML
    private TextField samplingRateField;
    @FXML
    private TextField quantizationLevelField;
    @FXML
    private TextField recreationField;
    @FXML
    private TextField resultField1;
    @FXML
    private TextField resultField2;
    @FXML
    private TextField resultField3;
    @FXML
    private TextField resultField4;
    @FXML
    private TextField resultField5;
    @FXML
    private ComboBox recreationMethodPicker;
    @FXML
    private Button quantizationBtn;
    @FXML
    private Button recreateBtn;

    private Integer chosenSignal;
    private ArrayList<Noise> generatedSignals = new ArrayList<>();

    private Noise actualNoise;

    private Noise lastSignalBeforeSampling;
    private FileManagement<Noise> fileManagement = new FileManagement<>();

    public ExampleChartController() {
    }


    public void initialize() {
        rangesAmountSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (actualNoise != null) {
                changeHistogram();
            }

        });

        signalChoice.getItems().addAll(
                "(S01) Szum o rozkladzie jednostajnym",
                "(S02) Szum gaussowski",
                "(S03) Sygnal sinusoidalny",
                "(S04) Sygnal sinusoidalny wyprostowany jednopolowkowo",
                "(S05) Sygnal sinusoidalny wyprostowany dwupolowkowo",
                "(S06) Sygnal prostokatny",
                "(S07) Sygnal prostokatny symetryczny",
                "(S08) Sygnal trojkatny",
                "(S09) Skok jednostkowy",
                "(S10) Impuls jednostkowy",
                "(S11) Szum impulsowy"
        );

        amplitudeValue.setDisable(true);
        periodValue.setDisable(true);
        beginTimeValue.setDisable(true);
        durationTimeValue.setDisable(true);
        linerValue.setDisable(true);
        samplingValue.setDisable(true);

        stepValue.setDisable(true);
        probabilityValue.setDisable(true);
        rangesAmountSlider.setDisable(true);

        rangesAmountSlider.setMin(5);
        rangesAmountSlider.setMax(20);
        rangesAmountSlider.setValue(5);
        rangesAmountSlider.setShowTickLabels(true);
        rangesAmountSlider.setShowTickMarks(true);
        rangesAmountSlider.setMajorTickUnit(1);
        rangesAmountSlider.setSnapToTicks(true);
        rangesAmountSlider.setMinorTickCount(0);
        rangesAmountSlider.setBlockIncrement(1);

        averageValueField.setDisable(true);
        averageAbsoluteValueField.setDisable(true);
        efficiencyValueField.setDisable(true);
        varianceField.setDisable(true);
        averagePowerField.setDisable(true);
        resultField1.setDisable(true);
        resultField2.setDisable(true);
        resultField3.setDisable(true);
        resultField4.setDisable(true);
        resultField5.setDisable(true);

        switchConverterAC(true);
        switchConverterCA(true);


        operationPicker.getItems().addAll("addition",
                "subtraction",
                "multiplication",
                "division");

        recreationMethodPicker.getItems().addAll("FOH", "sinc");
    }

    public void signalChoiceListener() {
        chosenSignal = signalChoice.getSelectionModel().getSelectedIndex() + 1;

        switch (chosenSignal) {
            case 1:
            case 2: {
                amplitudeValue.setDisable(false);
                periodValue.setDisable(true); //true
                beginTimeValue.setDisable(false);
                durationTimeValue.setDisable(false);
                linerValue.setDisable(true);
                samplingValue.setDisable(false);
                rangesAmountSlider.setDisable(false);
                probabilityValue.setDisable(true);
                stepValue.setDisable(true);
                break;
            }
            case 3:
            case 4:
            case 5: {
                amplitudeValue.setDisable(false);
                periodValue.setDisable(false);
                beginTimeValue.setDisable(false);
                durationTimeValue.setDisable(false);
                linerValue.setDisable(true);
                samplingValue.setDisable(false);
                rangesAmountSlider.setDisable(false);
                stepValue.setDisable(true);
                probabilityValue.setDisable(true);
                break;
            }
            case 6:
            case 7:
            case 8: {
                amplitudeValue.setDisable(false);
                periodValue.setDisable(false);
                beginTimeValue.setDisable(false);
                durationTimeValue.setDisable(false);
                linerValue.setDisable(false);
                samplingValue.setDisable(false);
                rangesAmountSlider.setDisable(false);
                stepValue.setDisable(true);
                probabilityValue.setDisable(true);
                break;
            }
            case 9:
            case 10: {
                amplitudeValue.setDisable(false);
                periodValue.setDisable(true);
                beginTimeValue.setDisable(false);
                durationTimeValue.setDisable(false);
                linerValue.setDisable(true);
                samplingValue.setDisable(false);
                rangesAmountSlider.setDisable(false);
                stepValue.setDisable(false);
                probabilityValue.setDisable(true);
                break;
            }
            case 11: {
                amplitudeValue.setDisable(false);
                periodValue.setDisable(true);
                beginTimeValue.setDisable(false);
                durationTimeValue.setDisable(false);
                linerValue.setDisable(true);
                samplingValue.setDisable(false);
                rangesAmountSlider.setDisable(false);
                stepValue.setDisable(true);
                probabilityValue.setDisable(false);
                break;
            }
        }
    }

    public void signalListListener() {
        lineChart2.getStylesheets().clear();
        signalChoiceListener();
        int choosen = signalsList.getSelectionModel().getSelectedIndex();
//        XYChart.Series<Number, Number> series1 = new XYChart.Series<>();
        actualNoise = generatedSignals.get(choosen);//przypisanie noise do tej zmiennej pozwala na zmianę histogramu w dowolnym momencie
        createChart();
        changeHistogram();
    }

    public void changeHistogram() {
        XYChart.Series<String, Number> series2 = new XYChart.Series();
        ArrayList<HistogramData> hst = actualNoise.histogramAnalyzer((int) rangesAmountSlider.getValue());
        averageValueField.setText(actualNoise.averageValue() + "");
        averageAbsoluteValueField.setText(actualNoise.averageAbsoluteValue() + "");
        efficiencyValueField.setText(actualNoise.efficiencyValue() + "");
        varianceField.setText(actualNoise.variance() + "");
        averagePowerField.setText(actualNoise.averagePower() + "");
        hst.forEach(v -> series2.getData().add(new XYChart.Data((Math.round(v.getBegin() * 100) / 100.0) + " to " + (Math.round(v.getEnd() * 100) / 100.0), v.getQty())));
        barChart1.getXAxis().setAnimated(false);
        barChart1.getData().clear();
        barChart1.layout();
        barChart1.getData().add(series2);

    }


    public void generateCharts() {

        lineChart2.getStylesheets().clear();
        // sprawdzenie, czy uzytkownik wybral typ wykresu
        if (signalChoice.getSelectionModel().getSelectedItem() != null) {

            // inicjalizacja serii sygnalu, serii danych, macierzy wartosci do wykresow

            Noise noise = new Noise(0, 0, 0);


            switch (chosenSignal) {
                case 1: {
                    noise = new UniformNoise(Double.parseDouble(amplitudeValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()));
                    break;
                }
                case 2: {
                    noise = new GausseNoise(Double.parseDouble(amplitudeValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()));
                    break;
                }
                case 3: {
                    noise = new SinusoidalSignal(Double.parseDouble(amplitudeValue.getText()),
                            1 / Double.parseDouble(periodValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()));
                    break;
                }
                case 4: {
                    noise = new SinusoidalSignalStraightHalf(Double.parseDouble(amplitudeValue.getText()),
                            1 / Double.parseDouble(periodValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()));
                    break;
                }
                case 5: {
                    noise = new SinusoidalSignalStraight(Double.parseDouble(amplitudeValue.getText()),
                            1 / Double.parseDouble(periodValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()));
                    break;
                }
                case 6: {
                    noise = new SquareWaveSignal(Double.parseDouble(amplitudeValue.getText()),
                            1 / Double.parseDouble(periodValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()), Double.parseDouble(linerValue.getText()));
                    break;
                }
                case 7: {
                    noise = new SquareSymmetricWaveSignal(Double.parseDouble(amplitudeValue.getText()),
                            1 / Double.parseDouble(periodValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()), Double.parseDouble(linerValue.getText()));
                    break;
                }
                case 8: {
                    noise = new TriangularSignal(Double.parseDouble(amplitudeValue.getText()),
                            1 / Double.parseDouble(periodValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()),
                            Double.parseDouble(linerValue.getText()));
                    break;
                }
                case 9: {
                    noise = new UnitStep(Double.parseDouble(amplitudeValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()),
                            Double.parseDouble(stepValue.getText()));
                    break;
                }
                case 10: {
                    noise = new UnitImpulse(Double.parseDouble(amplitudeValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()), Double.parseDouble(stepValue.getText()));
//                    zlineChart2.getStylesheets().add("Chart.css");
                    noise.setDiscrete(true);
                    break;
                }
                case 11: {
                    noise = new ImpulseNoise(Double.parseDouble(amplitudeValue.getText()),
                            Double.parseDouble(beginTimeValue.getText()),
                            Double.parseDouble(durationTimeValue.getText()), Double.parseDouble(probabilityValue.getText()));
//                    lineChart2.getStylesheets().add("Chart.css");
                    noise.setDiscrete(true);

                    break;
                }
            }

            noise.compute(1 / Double.parseDouble(samplingValue.getText()));
            actualNoise = noise; //przypisanie noise do tej zmiennej pozwala na zmianę histogramu w dowolnym momencie
            addNewNoiseToList(noise, noise.getClass().getSimpleName());
            createChart();
            changeHistogram();
            switchConverterAC(false);

        } else {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Error message");
            a.setHeaderText("Error!");
            a.setContentText("Please choose one of the signals from list.");
            a.show();
        }

    }

    public void switchConverterAC(boolean state) {
        samplingRateField.setDisable(state);
        quantizationLevelField.setDisable(state);
        quantizationBtn.setDisable(state);

    }

    public void switchConverterCA(boolean state) {
        recreationMethodPicker.setDisable(state);
        recreationField.setDisable(state);
        recreateBtn.setDisable(state);
    }

    public void addNewNoiseToList(Noise noise, String className) {
        generatedSignals.add(noise);
        String addedSignalInfo = generatedSignals.size() + ". "
                + className
                + " | Amplitude: "
                + noise.getAmpl()
                + " | Duration: "
                + noise.getDuration()
                + " | StartT: "
                + noise.getStartT();
        signalsList.getItems().add(addedSignalInfo);
        firstSignalList.getItems().add(addedSignalInfo);
        secondSignalList.getItems().add(addedSignalInfo);
        signalsList.getSelectionModel().selectLast();

    }

    public void createChart() {
        if(actualNoise.isDiscrete()){
            lineChart2.getStylesheets().add("Chart.css");
            lineChart2.setCreateSymbols(true);
        }
        XYChart.Series<Number, Number> series1 = new XYChart.Series<>();

        actualNoise.getData().forEach(v -> series1.getData().add(new XYChart.Data<>(v.x, v.y)));
        lineChart2.getData().clear();
        lineChart2.layout();
        lineChart2.getData().add(series1);
    }

    public void loadBtnListener() {
        rangesAmountSlider.setDisable(false);
        try {
            FileChooser fileChooser = new FileChooser();

            fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "\\Desktop"));

            // Wczytanie pliku pdf
            File file = fileChooser.showOpenDialog(exampleChartPane.getScene().getWindow());

            Noise noise = fileManagement.readFromBinFile(file.getAbsolutePath());
            actualNoise = noise;
            addNewNoiseToList(noise, file.getName());
            createChart();
            changeHistogram();
            switchConverterAC(false);
        } catch (IOException | NullPointerException | ClassNotFoundException e) {

        }
    }

    public void saveBtnListener() {
        try {
            if (actualNoise != null) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "\\Desktop"));
                File file = fileChooser.showSaveDialog(exampleChartPane.getScene().getWindow());

                fileManagement.writeToBinFile(actualNoise, file.getAbsolutePath());
            }
        } catch (IOException | NullPointerException e) {
        }
    }

    public void executeCalculation() throws NotSameSizeException, NotSameFrequencyException {
        int chosenOperation = operationPicker.getSelectionModel().getSelectedIndex();
        Integer chosen1 = firstSignalList.getSelectionModel().getSelectedIndex();

        Noise firstNoise = generatedSignals.get(chosen1);
        Integer chosen2 = secondSignalList.getSelectionModel().getSelectedIndex();
        Noise secondNoise = generatedSignals.get(chosen2);
        Operations operations = new Operations();
        Noise finalNoise = operations.compute(firstNoise, secondNoise, chosenOperation);

        actualNoise = finalNoise;
        addNewNoiseToList(finalNoise, finalNoise.getClass().getSimpleName() + " Enumerated");
        createChart();
        changeHistogram();

    }
    int fohMulitplication = 0;

    public void sampling() {
        ConverterAC converterAC = new ConverterAC();
        lastSignalBeforeSampling = actualNoise;
        Noise sampled = converterAC.sampling(1 / Double.parseDouble(samplingRateField.getText()), (Signal) actualNoise);
        fohMulitplication = (int) ((1/actualNoise.getSamplingRate())/Double.parseDouble(samplingRateField.getText()));
//        System.out.println("fohmultiplication "+fohMulitplication);

        actualNoise = sampled;
        addNewNoiseToList(sampled, "Sampled signal");
        sampled.setDiscrete(true);
        lineChart2.getStylesheets().add("Chart.css");
        createChart();
        changeHistogram();
        switchConverterCA(false);
    }


    public void quantize() {
        lineChart2.getStylesheets().clear();
        ConverterAC converterAC = new ConverterAC();
        Noise quantized = converterAC.quantization(actualNoise, Integer.parseInt(quantizationLevelField.getText()));
        actualNoise = quantized;
//        actualNoise.setDiscrete(true);
        addNewNoiseToList(quantized, "Quantized signal");
        quantized.setDiscrete(true);

        createChart();
        changeHistogram();
        switchConverterCA(false);
    }

    public void recreate() throws NotSameSizeException {
        int chosen = recreationMethodPicker.getSelectionModel().getSelectedIndex();
        ConverterCA converterCA = new ConverterCA();
        int value = 20;
        if (!recreationField.getText().isEmpty()) {
            value = Integer.parseInt(recreationField.getText());
        }
        lineChart2.getStylesheets().clear();
        Noise recreated = new Noise();
        switch (chosen) {
            case 0 -> {

                recreated = converterCA.interpolate(actualNoise, fohMulitplication);
                actualNoise = recreated;
                addNewNoiseToList(recreated, "Recreated signal");
                lineChart2.setCreateSymbols(false);
                createChart();
                changeHistogram();
            }
            case 1 -> {
                recreated = converterCA.interSinc(actualNoise,value,lastSignalBeforeSampling.getData().size());
                actualNoise=recreated;

                addNewNoiseToList(recreated,"Recreated sinc signal");
                lineChart2.setCreateSymbols(false);
                createChart();
                changeHistogram();
            }

        }

        lineChart2.getStylesheets().clear();
        // TODO usunac
//        // testy
        System.out.println("generated: "+ generatedSignals.get(0).getData().size());
        System.out.println("restored: "+ actualNoise.getData().size());
        double MSE = signalErrors.countMSE(lastSignalBeforeSampling.getData(), actualNoise.getData());
        double SNR = signalErrors.countSNR(lastSignalBeforeSampling.getData(), actualNoise.getData());
        double PSNR = signalErrors.countPSMR(lastSignalBeforeSampling.getData(), actualNoise.getData());
        double MD = signalErrors.countMD(lastSignalBeforeSampling.getData(), actualNoise.getData());
        double enob = signalErrors.countENOB(lastSignalBeforeSampling.getData(), actualNoise.getData());

        resultField1.setText(MSE+"");
        resultField2.setText(SNR+"");
        resultField3.setText(PSNR+"");
        resultField4.setText(MD+"");
        resultField5.setText(enob+"");

    }
}
