package pl.pmms.cps.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class MainFrameController {

    @FXML
    private Pane mainFramePane;

    @FXML
    public void initialize() {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/ExampleChart.fxml"));
        Pane exampleChart = null;
        try {
            exampleChart = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainFramePane.getChildren().add(exampleChart);
    }


}
