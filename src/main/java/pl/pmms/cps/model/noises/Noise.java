package pl.pmms.cps.model.noises;

import pl.pmms.cps.model.HistogramData;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 * @author Patryk Markowski
 */
public class Noise implements Serializable {
    private ArrayList<Point2D.Double> data = new ArrayList<>();
    private double ampl; //amplitude
    private double startT; // czas startowy
    private double duration; //czas trwania
    private boolean isDiscrete = false; // sprawdzenie rodzaju sygnalu, do wygenerowania wykresu
    private double samplingRate = 0;
    public Noise() {

    }

    public Noise(double ampl, double startT, double duration) {
        this.ampl = ampl;
        this.startT = startT;
        this.duration = duration;
    }

    //TODO można zmienić na void zapisać w data i przy wywoływaniu odpalić compute i potem getData -- ułatwi zapis i odczyt z pliku

    public ArrayList<HistogramData> histogramAnalyzer(int rangesAmount) {
        ArrayList<HistogramData> histogramData = new ArrayList<>();
        Double min = getActualData().stream().min(Comparator.comparing(Point2D.Double::getY)).orElseThrow(NoSuchElementException::new).y;
        Double max = getActualData().stream().max(Comparator.comparing(Point2D.Double::getY)).orElseThrow(NoSuchElementException::new).y;
        Double step = (max - min) / rangesAmount;

        for (int i = 0; i < rangesAmount; i++) {
            final Double beg = min;
            final Double end = min + step;
            Integer amount = Math.toIntExact(getActualData().stream().filter(d -> d.y >= beg && d.y <= end).count());
            HistogramData hstdta = new HistogramData(min, min + step, amount);
            histogramData.add(hstdta);
            min += step;
        }

        return histogramData;
    }

    public void compute(double freq) {
        this.setSamplingRate(freq);
        ArrayList<Point2D.Double> values = new ArrayList<>();
        for (double i = startT; i < startT + duration; i += freq) {
            values.add(new Point2D.Double(i, calculate(i)));
        }
        data = values;
    }

    public double averageValue() {
        double value = 0;

        for (Point2D.Double v : getActualData()) {
            value += v.y;
        }

        return value / getActualData().size();
    }

    public double averageAbsoluteValue() {
        double value = 0;

        for (Point2D.Double v : getActualData()) {
            value += Math.abs(v.y);
        }
        return value / getActualData().size();

    }


    public double averagePower() {
        double value = 0;
        for (Point2D.Double v : getActualData()) {
            value += (v.y * v.y);
        }
        return value / getActualData().size();

    }

    public double variance() {
        double value = 0;
        double mean = averageValue();
        for (Point2D.Double v : getActualData()) {
            double tmp = v.y - mean;
            value += (tmp * tmp);
        }
        return value / getActualData().size();
    }

    public double efficiencyValue() {
        return Math.sqrt(averagePower());
    }

    public double calculate(double t) {
        return 1;
    }

    public ArrayList<Point2D.Double> getData() {
        return data;
    }

    public void setData(ArrayList<Point2D.Double> data) {
        this.data = data;
    }

    public double getAmpl() {
        return ampl;
    }

    public void setAmpl(double ampl) {
        this.ampl = ampl;
    }

    public double getStartT() {
        return startT;
    }

    public double getDuration() {
        return duration;
    }

    public ArrayList<Point2D.Double> getActualData() {
        return getData();
    }

    public boolean isDiscrete() {
        return isDiscrete;
    }

    public void setDiscrete(boolean discrete) {
        isDiscrete = discrete;
    }

    public double getSamplingRate() {
        return samplingRate;
    }

    public void setSamplingRate(double samplingRate) {
        this.samplingRate = samplingRate;
    }
}
