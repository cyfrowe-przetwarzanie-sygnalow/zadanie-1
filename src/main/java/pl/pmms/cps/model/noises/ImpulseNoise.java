package pl.pmms.cps.model.noises;

import java.util.Random;

/**
 * @author Patryk Markowski
 */
public class ImpulseNoise extends Noise{
//    private final Double freq;
    private final Double p; //prawdopodobieństwo
    private final Random rand = new Random();

    public ImpulseNoise(double ampl, double startT, double duration, Double p) {
        super(ampl, startT, duration);
        this.p = p;
    }

    @Override
    public double calculate(double t) {
        if (rand.nextDouble() < p){
            return getAmpl();
        }
        else {
            return 0.0;
        }
    }
}
