package pl.pmms.cps.model.noises;

/**
 * @author Patryk Markowski
 */
public class UnitStep extends Noise{
    private final Double switchT;

    public UnitStep(double ampl, double startT, double duration, Double switchT) {
        super(ampl, startT, duration);
        this.switchT = switchT;
    }

    @Override
    public double calculate(double t) {
        if(t<switchT){return 0;}
        if(t==switchT){return getAmpl()/2;}
        return getAmpl();
    }
}
