package pl.pmms.cps.model.noises;

import java.util.Random;

/**
 * @author Patryk Markowski
 */
public class GausseNoise extends Noise{
    private final Random rand = new Random();
    public GausseNoise(double ampl, double startT, double duration) {
        super(ampl, startT, duration);
    }

    @Override
    public double calculate(double t) {
        return rand.nextGaussian();
//        return -getAmpl()+(2*getAmpl())*rand.nextGaussian();
    }
}
