package pl.pmms.cps.model.noises;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author Patryk Markowski
 */
public class UniformNoise extends Noise{

    private final Random rand = new Random();
    public UniformNoise(double ampl, double startT, double duration) {
        super(ampl, startT, duration);
    }

    @Override
    public double calculate(double t) {
        return -getAmpl()+(2*getAmpl())*rand.nextDouble();
    }
}
