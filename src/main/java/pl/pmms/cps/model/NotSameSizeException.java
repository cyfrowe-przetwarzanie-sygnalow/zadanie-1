package pl.pmms.cps.model;

/**
 * @author Patryk Markowski
 */
public class NotSameSizeException extends Exception{
    public NotSameSizeException(String message) {
        super(message);
    }

}
