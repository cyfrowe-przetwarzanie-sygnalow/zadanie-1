package pl.pmms.cps.model.signals;

import pl.pmms.cps.model.signals.Signal;

/**
 * @author Patryk Markowski
 */
public class SquareSymmetricWaveSignal extends Signal {
    private final double fill;

    public SquareSymmetricWaveSignal(double ampl, double period, double startT, double duration, double fill) {
        super(ampl, period, startT, duration);
        this.fill = fill;
    }

    @Override
    public double calculate(double t) {
        if (((t - getStartT()) / getPeriod()) - Math.floor((t - getStartT()) / getPeriod()) < fill) {
            return getAmpl();
        } else {
            return -getAmpl();
        }
    }
}
