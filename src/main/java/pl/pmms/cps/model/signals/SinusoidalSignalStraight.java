package pl.pmms.cps.model.signals;

import pl.pmms.cps.model.signals.Signal;

/**
 * @author Patryk Markowski
 */
public class SinusoidalSignalStraight extends Signal {
    public SinusoidalSignalStraight(double ampl, double period, double startT, double duration) {
        super(ampl, period, startT, duration);
    }

    @Override
    public double calculate(double t) {
        return getAmpl() * Math.abs(Math.sin((2 * Math.PI / getPeriod()) * (t - getStartT())));
    }
}
