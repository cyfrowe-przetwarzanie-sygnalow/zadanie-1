package pl.pmms.cps.model.signals;

import pl.pmms.cps.model.signals.Signal;

/**
 * @author Patryk Markowski
 */

public class SinusoidalSignal extends Signal {

    public SinusoidalSignal(double ampl, double period, double startT, double duration) {
        super(ampl, period, startT, duration);
    }

    @Override
    public double calculate(double t) {

        return getAmpl() * Math.sin((2 * Math.PI / getPeriod()) * (t - getStartT()));
    }





}
