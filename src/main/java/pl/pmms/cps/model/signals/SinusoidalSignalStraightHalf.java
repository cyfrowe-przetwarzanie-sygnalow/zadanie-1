package pl.pmms.cps.model.signals;

import pl.pmms.cps.model.signals.Signal;

/**
 * @author Patryk Markowski
 */
public class SinusoidalSignalStraightHalf extends Signal {

    public SinusoidalSignalStraightHalf(double ampl, double period, double startT, double duration) {
        super(ampl, period, startT, duration);
    }

    @Override
    public double calculate(double t) {
        return 0.5 * getAmpl() * (
                Math.sin((2 * Math.PI / getPeriod()) * (t - getStartT()))
                        + Math.abs(Math.sin((2 * Math.PI / getPeriod()) * (t - getStartT())))
        );
    }
}
