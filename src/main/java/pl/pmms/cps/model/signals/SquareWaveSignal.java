package pl.pmms.cps.model.signals;

import pl.pmms.cps.model.signals.Signal;

/**
 * @author Patryk Markowski
 */
public class SquareWaveSignal extends Signal {
    private final double fill; //wypełnienie

    public SquareWaveSignal(double ampl, double period, double startT, double duration, double fill) {
        super(ampl, period, startT, duration);
        this.fill = fill;
    }

    @Override
    public double calculate(double t) {
        if (((t - getStartT()) / getPeriod()) - Math.floor((t - getStartT()) / getPeriod()) < fill) {
            return getAmpl();
        } else {
            return 0.0;
        }
    }
}
