package pl.pmms.cps.model.signals;

//import lombok.AllArgsConstructor;

import pl.pmms.cps.model.signals.Signal;

/**
 * @author Patryk Markowski
 */

public class TriangularSignal extends Signal {
public final double fill;

    public TriangularSignal(double ampl, double period, double startT, double duration, double fill) {
        super(ampl, period, startT, duration);
        this.fill = fill;
    }

    @Override
    public double calculate(double t) {

        double termPosition = ((t - getStartT()) / getPeriod()) - Math.floor((t - getStartT()) / getPeriod());
        if (termPosition < fill) {
            return termPosition / fill * getAmpl();
        } else {
            return (1 - (termPosition - fill) / (1 - fill)) * getAmpl();
        }
    }
}
