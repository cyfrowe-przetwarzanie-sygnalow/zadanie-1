package pl.pmms.cps.model.signals;

import pl.pmms.cps.model.noises.Noise;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author Patryk Markowski
 */

public class Signal extends Noise {
    private final double period; // długość okresu

    public Signal(double ampl, double period, double startT, double duration) {
        super(ampl, startT, duration);
        this.period = period;
    }


    @Override
    public double calculate(double t) {
        return 1;
    }

    @Override
    public ArrayList<Point2D.Double> getActualData(){
        return cutDataForCalculations();
    }

    //przyciecie danych tylko do pelnych okresow
    public ArrayList<Point2D.Double> cutDataForCalculations() {
        double numbersOfFullPeriods =  (getDuration() / period);
        double fullPeriodsTime =  (numbersOfFullPeriods * period);

        return getData().stream().filter(v -> v.x <= fullPeriodsTime).collect(Collectors.toCollection(ArrayList::new));
    }

    public double getPeriod() {
        return period;
    }

}
