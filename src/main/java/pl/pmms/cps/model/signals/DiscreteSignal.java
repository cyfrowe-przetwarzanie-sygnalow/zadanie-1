package pl.pmms.cps.model.signals;

import pl.pmms.cps.model.noises.Noise;

import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * @author Patryk Markowski
 */
public class DiscreteSignal extends Noise {

//    private ArrayList<Point2D.Double>  samples = new ArrayList<>();

    public DiscreteSignal() {
        super();
        setDiscrete(true);
    }

//    public ArrayList<Point2D.Double> getSamples() {
//        return samples;
//    }
//
//    public void setSamples(ArrayList<Point2D.Double> samples) {
//
//        this.samples = samples;
//        this.setData(samples);
//    }
}
