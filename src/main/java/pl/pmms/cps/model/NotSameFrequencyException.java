package pl.pmms.cps.model;

/**
 * @author Patryk Markowski
 */
public class NotSameFrequencyException extends Exception {
    public NotSameFrequencyException(String message) {
        super(message);
    }
}
