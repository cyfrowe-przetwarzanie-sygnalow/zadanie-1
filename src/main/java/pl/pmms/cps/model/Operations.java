package pl.pmms.cps.model;
import pl.pmms.cps.model.noises.Noise;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.NoSuchElementException;

public class Operations {
    Noise noise;
    public Operations() {
    }
    public Noise compute(Noise first,Noise second,int operationNumber) throws NotSameSizeException, NotSameFrequencyException {
        ArrayList<Point2D.Double> firstData = first.getData();
        ArrayList<Point2D.Double>secondData= second.getData();
        if(firstData.size()!=secondData.size())
        {
        throw new NotSameSizeException("Signals lengths are not the equals!");
        }
        if(firstData.get(1).x-firstData.get(0).x != secondData.get(1).x-secondData.get(0).x )
        {
            throw new NotSameFrequencyException("Signals frequencies are not equals!");
        }
        ArrayList<Point2D.Double> finalData = new ArrayList<>();
        switch (operationNumber) {
            case 0:
                for(int i = 0;i<firstData.size();i++)
                {
                    Point2D.Double tmp = new Point2D.Double();
                    tmp.x=firstData.get(i).x;
                    tmp.y=firstData.get(i).y+secondData.get(i).y;
                    finalData.add(tmp);
                }
                break;
            case 1:
                for(int i = 0;i<firstData.size();i++)
                {
                    Point2D.Double tmp = new Point2D.Double();
                    tmp.x=firstData.get(i).x;
                    tmp.y=firstData.get(i).y-secondData.get(i).y;
                    finalData.add(tmp);
                }
                break;
            case 2:
                for(int i = 0;i<firstData.size();i++)
                {
                    Point2D.Double tmp = new Point2D.Double();
                    tmp.x=firstData.get(i).x;
                    tmp.y=firstData.get(i).y*secondData.get(i).y;
                    finalData.add(tmp);
                }
                break;
            case 3:
                for(int i = 0;i<firstData.size();i++)
                {
                    Point2D.Double tmp = new Point2D.Double();
                    tmp.x=firstData.get(i).x;
                    if(firstData.get(i).y==0)
                    {
                        tmp.y=0;
                    }
                    else {
                        tmp.y = firstData.get(i).y / secondData.get(i).y;
                    }
                    finalData.add(tmp);
                }
                break;

        }
        Double ampl=(finalData.stream().max(Comparator.comparing(Point2D.Double::getY)).orElseThrow(NoSuchElementException::new).y);

        Noise finalNoise = new Noise(ampl,first.getStartT(),first.getDuration());
       finalNoise.setData(finalData);

        return finalNoise;
    }
}