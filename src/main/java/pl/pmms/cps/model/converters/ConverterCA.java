package pl.pmms.cps.model.converters;

import pl.pmms.cps.model.noises.Noise;
import pl.pmms.cps.model.signals.DiscreteSignal;
import pl.pmms.cps.model.signals.Signal;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.function.DoubleBinaryOperator;

/**
 * @author Patryk Markowski
 */
public class ConverterCA {

    public ConverterCA() {
    }

    public Noise interpolate(Noise signal, int accuracy) {
        Noise noise = new Noise();
        //accuracy - ilość próbek pomiędzy punktami
        ArrayList<Point2D.Double> interpolated = new ArrayList<>();
        // iteracja po wszystkich probkach, oprócz ostatniej
        for (int i = 1; i < signal.getData().size(); i++) {

            // y = ax + b
            // wyznaczenie wartosci wspolczynnikow a i b dla pojedynczej prostej pomiedzy punktem aktualnym i nastepnym
            double a = (signal.getData().get(i).y - signal.getData().get(i-1).y) / (signal.getData().get(i).x - signal.getData().get(i-1).x);
            double b = signal.getData().get(i-1).y - (a * signal.getData().get(i-1).x);
            double step = Math.abs(signal.getData().get(i).x - signal.getData().get(i-1).x) / (accuracy );
            for (int j = 1; j < accuracy+1 ; j++) {
                double x = signal.getData().get(i-1).x + (j * step);
                double y = a * x + b;
                Point2D.Double tmp = new Point2D.Double(x, y);
                interpolated.add(tmp);
            }

        }
        // ostatni punkt
        double a = (signal.getData().get(signal.getData().size()-1).y - signal.getData().get(signal.getData().size()-2).y) /
                    (signal.getData().get(signal.getData().size()-1).x - signal.getData().get(signal.getData().size()-2).x);
        double b = signal.getData().get(signal.getData().size()-1).y - (a * signal.getData().get(signal.getData().size()-1).x);
        double step = Math.abs(signal.getData().get(signal.getData().size()-1).x - signal.getData().get(signal.getData().size()-2).x) / (accuracy );

        for (int j = 1; j < accuracy+1 ; j++) {
            double x = signal.getData().get(signal.getData().size()-1).x + (j * step);
            double y = a * x + b;
            Point2D.Double tmp = new Point2D.Double(x, y);
            interpolated.add(tmp);
        }
        noise.setData(interpolated);
        return noise;
    }

    public Noise interSinc(Noise signalToInterpolate, int neighboursAmount, int originalSignalAmountOfProbes) {
        Noise sincFunctionInterpolated = new Noise();
        ArrayList<Point2D.Double> interpolatedSamples = new ArrayList<>(originalSignalAmountOfProbes);

        double signalDuration = signalToInterpolate.getData().get(signalToInterpolate.getData().size()-1).getX() - signalToInterpolate.getData().get(0).getX();
        double step = signalDuration / originalSignalAmountOfProbes;


        // wypelnienie wstepne z punktami na osi X
        for (int i = 0; i < originalSignalAmountOfProbes; i++) {
            Point2D.Double tmp = new Point2D.Double();
            tmp.x = step * i;
            tmp.y = value(tmp.x, neighboursAmount, signalToInterpolate);
            interpolatedSamples.add(tmp);
        }

        sincFunctionInterpolated.setData(interpolatedSamples);
        return sincFunctionInterpolated;
    }



    public double value(double t, int N, Noise signalToInterpolate) {

        double start = signalToInterpolate.getData().get(0).getX();
        double end = signalToInterpolate.getData().get(signalToInterpolate.getData().size()-1).getX();
        int index = (int) Math.floor((t - start) / end * signalToInterpolate.getData().size());

        int firstSample = index - N;
        int lastSample = index + N + 1;
        if (firstSample < 0) {
            lastSample = lastSample - firstSample;
            firstSample = 0;
            if (lastSample > signalToInterpolate.getData().size()) {
                lastSample = signalToInterpolate.getData().size();
            }
        } else if (lastSample > signalToInterpolate.getData().size()) {
            firstSample = firstSample - (lastSample - signalToInterpolate.getData().size());
            lastSample = signalToInterpolate.getData().size();
            if (firstSample < 0) {
                firstSample = 0;
            }
        }

        double step = end / signalToInterpolate.getData().size();

        double sum = 0.0;
        for (int i = firstSample; i < lastSample; i++) {
            sum += signalToInterpolate.getData().get(i).getY() * sinc(t / step - i);
        }
        return sum;
    }

    private double sinc (double t){
        if(Math.abs(t)<0.0001) return 1;
        return Math.sin((Math.PI*t))/(Math.PI*t);
    }
}
