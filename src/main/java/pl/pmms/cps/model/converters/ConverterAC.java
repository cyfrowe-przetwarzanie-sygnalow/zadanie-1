package pl.pmms.cps.model.converters;

import pl.pmms.cps.model.NotSameSizeException;
import pl.pmms.cps.model.noises.Noise;
import pl.pmms.cps.model.signals.DiscreteSignal;
import pl.pmms.cps.model.signals.Signal;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.AreaAveragingScaleFilter;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * @author Patryk Markowski
 */
public class ConverterAC {
    private double mse;
    private double snr;
    private double psnr;
    private double md;

    public ConverterAC() {
    }

    public DiscreteSignal sampling(double samplingRate, Signal signal) {
//        ArrayList<Point2D.Double> test = new ArrayList<>(); // testy
        DiscreteSignal discreteSignal = new DiscreteSignal();
        ArrayList<Point2D.Double> samples = new ArrayList<>();
        //ilość próbek na jeden okres

        int sampleIterator = (int) ((int) (1/signal.getSamplingRate())*samplingRate);

        for (int i = 0; i < signal.getData().size(); i+= sampleIterator){
            samples.add(signal.getData().get(i));
        }
        discreteSignal.setData(samples); // stare, przywrocic jak cos
        return discreteSignal;
    }


//    public Noise quantization2 (Noise signal, int bits){
//        // bits - liczba bitow uzytych w kwantyzacji
//        // Xmax, Xmin - maksymalna i minimalna wartosc sygnału
//        // L - poziomy kwantyzacji
//        // delta - ? opisac
//        // I - ? opisac
//
////        DiscreteSignal discreteSignal = new DiscreteSignal();
//        Noise noise = new Noise();
//        double I;
//        double Xmin = Double.MAX_VALUE, Xmax = Double.MIN_VALUE, delta;
//        int L = (int) Math.pow(2, bits);
//        System.out.println("L: " + L);
//
//        // znalezienie max i min wartosci
//        for (int i = 0; i < signal.getData().size(); i++){
//            if (signal.getData().get(i).getY() < Xmin){
//                Xmin = signal.getData().get(i).getY();
//            }
//            if (signal.getData().get(i).getY() > Xmax){
//                Xmax = signal.getData().get(i).getY();
//            }
//        }
//
//        delta = (Xmax - Xmin) / L;
//        //
////        System.out.println("Xmin: " + Xmin);
////        System.out.println("Xmax: " + Xmax);
////        System.out.println("delta: " + delta);
//        //
//        for (int j = 0; j < signal.getData().size(); j++){
//            I = (Math.floor(signal.getData().get(j).getY()-Xmin)/delta); // czy tu nie powinien byc floor?
////            I = (Math.round(signal.getData().get(j).getY()-Xmin)/delta); // czy tu nie powinien byc floor?
//            System.out.println("I : " + I);
//            if ( I == L) {
//                I = I - 1;
////                System.out.println("I == L: " + I);
//            }
//            if (I < 0){
//                I = 0;
////                System.out.println("I < 0: " + I);
//            }
//            signal.getData().get(j).y = Xmin + (I * delta);
//            //
////            System.out.println("////");
////            System.out.println("signal x: " + signal.getData().get(j).getX());
////            System.out.println("signal y: " + signal.getData().get(j).getY());
////            System.out.println("////");
//            //
//        }
//        noise.setData(signal.getData());
////        discreteSignal.setData(signal.getData());
////        return discreteSignal;
//        return noise;
//    }


    public DiscreteSignal quantization (Noise signal, int levels) {
        DiscreteSignal discreteSignal = new DiscreteSignal();
        ArrayList<Point2D.Double> quantizedSamples = new ArrayList<>();
        ArrayList<Double> levelsValue = calculateLevelsValue(signal, levels);

        signal.getData().forEach(v -> {
            double closest = 100000;
            int index = 0;

            for (int i = 0; i < levelsValue.size(); i++) {
                if (Math.abs(v.y - levelsValue.get(i)) < closest) {
                    closest = Math.abs(v.y - levelsValue.get(i));
                    index = i;
                }
            }
            Point2D.Double tmp = new Point2D.Double();
            tmp.x = v.x;
            tmp.y = levelsValue.get(index);
            quantizedSamples.add(tmp);
        });
        discreteSignal.setData(quantizedSamples);
        return discreteSignal;

    }

    public ArrayList<Double> calculateLevelsValue(Noise signal, int levels) {
        ArrayList<Double> levelsValues = new ArrayList<>();
        double step = (findMax(signal) - findMin(signal)) / (levels - 1);
        for (int i = 0; i < levels; i++) {
            levelsValues.add(step * i + findMin(signal));
        }
        return levelsValues;
    }

    private double findMax(Noise signal) {
        double max = Double.MIN_VALUE;
        for (Point2D.Double value : signal.getData()) {
            if (Math.floor(value.y) > max) max = value.y;
        }
        return max;
    }

    private double findMin(Noise signal) {
        double min = Double.MAX_VALUE;
        for (Point2D.Double value : signal.getData()) {
            if (Math.floor(value.y) < min) min = value.y;
        }
        return min;
    }

    private void countMSE(Noise signal, DiscreteSignal quantizated) throws NotSameSizeException {
        if (signal.getData().size() == quantizated.getData().size()) {
            double sum = 0;
            for (int i = 0; i < signal.getData().size(); i++) {
                sum += (signal.getData().get(i).y - quantizated.getData().get(i).y) * (signal.getData().get(i).y - quantizated.getData().get(i).y);
            }
            setMse(sum / signal.getData().size());

        }

    }

    public double getMse() {
        return mse;
    }

    public void setMse(double mse) {
        this.mse = mse;
    }

    public double getSnr() {
        return snr;
    }

    public void setSnr(double snr) {
        this.snr = snr;
    }

    public double getPsnr() {
        return psnr;
    }

    public void setPsnr(double psnr) {
        this.psnr = psnr;
    }

    public double getMd() {
        return md;
    }

    public void setMd(double md) {
        this.md = md;
    }
}
