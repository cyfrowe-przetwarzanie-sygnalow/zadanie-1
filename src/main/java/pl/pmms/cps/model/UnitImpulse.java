package pl.pmms.cps.model;

import pl.pmms.cps.model.noises.Noise;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 * @author Patryk Markowski
 */
public class UnitImpulse extends Noise {

    private final double stepProbe; //numer próbki


    public UnitImpulse(double ampl, double startT, double duration, double stepProbe) {
        super(ampl, startT, duration);
        this.stepProbe = stepProbe;
    }

    @Override
    public double calculate(double t) {
        if (Math.abs(stepProbe - t) <= 0.0000001) {
            return getAmpl();
        }
        return 0;
    }

    public double getStepProbe() {
        return stepProbe;
    }
}

