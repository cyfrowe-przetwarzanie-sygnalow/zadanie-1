package pl.pmms.cps.model;

import java.awt.geom.Point2D;
import java.io.*;
import java.util.ArrayList;

/**
 * @author Patryk Markowski
 */
public class FileManagement<T> {



    public void writeToBinFile(T object, String name) throws IOException {
        FileOutputStream fos = new FileOutputStream(name);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
    }

    public T readFromBinFile(String name) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(name);
        ObjectInputStream ois = new ObjectInputStream(fis);
        return (T)ois.readObject();
    }
}


/*
    public void writeToBinFile(String name, ArrayList<Point2D.Double> data) throws IOException {
        FileOutputStream fos = new FileOutputStream(name+".dat");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(data);
    }
    public ArrayList<Point2D.Double> readFromBinFile(String name) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(name);
        ObjectInputStream ois = new ObjectInputStream(fis);
        return (ArrayList<Point2D.Double>)ois.readObject();
    }
 */
