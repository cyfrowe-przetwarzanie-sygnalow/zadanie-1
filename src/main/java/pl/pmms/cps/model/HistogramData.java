package pl.pmms.cps.model;

public class HistogramData {

    private final double begin;
    private final double end;
    private final double qty;

    public HistogramData(double begin, double end, double qty) {
        this.begin = begin;
        this.end = end;
        this.qty = qty;
    }

    public double getBegin() {
        return begin;
    }

    public double getEnd() {
        return end;
    }

    public double getQty() {
        return qty;
    }

    @Override
    public String toString() {
        return "HistogramData{" +
                "begin=" + begin +
                ", end=" + end +
                ", qty=" + qty +
                '}';
    }
}
