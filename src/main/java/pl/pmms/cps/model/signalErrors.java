package pl.pmms.cps.model;

import pl.pmms.cps.model.noises.Noise;
import pl.pmms.cps.model.signals.DiscreteSignal;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 * @author Patryk Markowski
 */
public class signalErrors {

    public static double countMSE(ArrayList<Point2D.Double> original , ArrayList<Point2D.Double> restored) throws NotSameSizeException {
        if(original.size()==restored.size()){
            double sum = 0;
            for(int i=0;i<original.size();i++){
                sum+=(original.get(i).y-restored.get(i).y)*(original.get(i).y-restored.get(i).y);
            }
            return (sum/original.size());

        }
        throw new NotSameSizeException("original and restored signals are not same size");

    }
    public static double countSNR(ArrayList<Point2D.Double> original , ArrayList<Point2D.Double> restored) throws NotSameSizeException {
        double MSE = countMSE(original,restored)*original.size(); /// mse multiplied with size
        double sum = original.stream().mapToDouble(v -> v.y * v.y).sum();
        return 10*Math.log10(sum/MSE);

    }
    public static double countPSMR( ArrayList<Point2D.Double> original , ArrayList<Point2D.Double> restored) throws NotSameSizeException {
        Double max = original.stream().max(Comparator.comparing(Point2D.Double::getY)).orElseThrow(NoSuchElementException::new).y;
        double MSE = countMSE(original,restored);
        return 10*Math.log10(max/MSE);

    }
    public static double countMD (ArrayList<Point2D.Double> original , ArrayList<Point2D.Double> restored) throws NotSameSizeException {
        ArrayList<Double> difference = new ArrayList<Double>();
        if(original.size()==restored.size()){
            for(int i=0;i<original.size();i++){
                difference.add(Math.abs(original.get(i).y-restored.get(i).y));
            }
            return difference.stream().max(Double::compare).orElseThrow(NoSuchElementException::new);

        }
        throw new NotSameSizeException("original and restored signals are not same size");

    }
    public static double countENOB(ArrayList<Point2D.Double> original , ArrayList<Point2D.Double> restored) throws NotSameSizeException {
        return (countSNR(original,restored)-1.76)/6.02;
    }
}
