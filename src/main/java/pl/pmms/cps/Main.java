package pl.pmms.cps;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pl.pmms.cps.model.converters.ConverterAC;
import pl.pmms.cps.model.signals.DiscreteSignal;
import pl.pmms.cps.model.signals.SinusoidalSignal;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainFrame.fxml"));
        Pane mainPane = loader.load();
        Scene scene = new Scene(mainPane);

        stage.setScene(scene);
        stage.setTitle("CPS");
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);



    }
}